# SAPIENTA CLI Tool 

This is a very simple commandline tool for accessing SAPIENTA for batch processing/annotation of papers.

## Installation

To install this script you will need Python 2.7.X with distutils.

 1. Download and clone the repository `git clone https://partridge@bitbucket.org/partridge/sapienta_wsclient.git`
 2. Run `python setup.py install`
 3. You can now run the tool `sapientacli -a your_paper.xml`. See below for arguments and examples


## Running the tool

The tool communicates with the SAPIENTA processing cluster over websockets. You can convert PDF to XML, sentence split and annotate your pappers in batch  using this system. 

### PDF to XML

If you pass in a PDF file with no arguments the default action is to use [PDFX](http://pdfx.cs.man.ac.uk/) to convert your paper to DoCo XML. If you pass in annotate (-a) or split (-s) then your PDF will be converted to XML and then these actions applied on top.

Example:

    sapientacli mypdfpaper.pdf

### Sentence Splitting

By passing the -s switch your paper will be sentence segmented - each sentence is denoted by an <s> element.

Example XML:

    sapientacli -s mypaper.xml

### SAPIENTA CoreSC annotation

By passing the -a switch your paper will be converted (if necessary), sentence segmented and then annotated with CoreSC labels. 

Example annotation call:

    sapientacli -a mypaper.xml

Each sentence is wrapped in a <CoreSC1 type="XXX"> tag where XXX is the CoreSC. Possible annotations are:

 * Background  (Bac)
 * Conclusions (Con)
 * Experiment (Exp)
 * Goal (Goa)
 * Hypothesis (Hyp)
 * Model (Mod)
 * Method (Met)
 * Motivation (Mot)
 * Object (Obj)
 * Observation (Obs)
 * Result (Res)

## Batch Processing

To use sapientacli for batch processing simply pass in a wildcard argument for paper names or a long list of papers like so:

(Note that the script doesn't care if you pass in a mix of pdf and xml files)

    sapientacli -a papersdir/*.xml

or

    sapientacli -a paper1.pdf paper2.xml paper3.xml paper4.pdf paper5.pdf paper6.xml


## Host and port

In order to use a SAPIENTA instance hosted somewhere other than sapienta.papro.org.uk, use the host and port switches:

    sapientacli --host example.com --p 8080 -a paper1.xml