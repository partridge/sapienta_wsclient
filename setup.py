from setuptools import setup, find_packages

setup(
    name = "SAPIENTA-wscli",
    version = 0.1,
    packages = find_packages(),

    #install requirements
    install_requires = [
            'socketIO-client>=0.6.5'],

    entry_points = {"console_scripts" : [
        'sapientacli = sapientacli:main',
        ]},

    author="James Ravenscroft",
    author_email = "ravenscroft@papro.org.uk",
    description = "Toolkit for annotating XML scientific papers with CoreSC labels",
    url="http://www.sapientaproject.com/"
        
)
